import secrets


def get_nonce():
    return secrets.randbelow(1000000000) + 10000
