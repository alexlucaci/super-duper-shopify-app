from .shopify_manager import ShopifyManager


class StoreProperties(ShopifyManager):
    """ Shopify Manager to handle store properties requests
    described in https://help.shopify.com/en/api/reference/store-properties
    """

    def get_properties(self):
        endpoint = "shop.json"

        return self.do_get(endpoint=endpoint).get("shop")
