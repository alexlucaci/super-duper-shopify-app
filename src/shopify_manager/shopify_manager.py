import logging

import requests
from rest_framework import exceptions

LOGGER = logging.getLogger(__name__)


class ShopifyManagerException(exceptions.APIException):
    def __init__(self, error_message, error_code):
        super(ShopifyManagerException, self).__init__(error_message, error_code)


class ShopifyManager:
    # Error codes from https://help.shopify.com/en/api/getting-started/response-status-codes
    __ERROR_CODES = [303, 400, 401, 402, 403, 404, 406, 422, 423, 429, 500, 501, 503, 504]
    _POST = "POST"
    _GET = "GET"
    _DELETE = "DELETE"
    _PUT = "PUT"
    _PATCH = "PATCH"

    def __init__(self, store, access_token, version="2020-01"):
        self.store = store
        self.access_token = access_token
        self.version = version

        self.headers = {
            "X-Shopify-Access-Token": access_token,
            "Accept": "application/json",
            "Content-Type": "application/json",
        }

    def make_request(self, method, endpoint, payload=None, headers=None):
        if headers is None:
            headers = self.headers

        url = f"https://{self.store}/admin/api/{self.version}/{endpoint}"
        response = requests.request(
            method=method, url=url, headers=headers, verify=False, json=payload, timeout=30, **{}
        )

        return response

    def handle_response(self, response):
        """ Check if the status code is matching the error codes one
        :param response:
        :return:
        """

        if response is None:
            raise ShopifyManagerException("Something went wrong with Shopify API. Reponse is None", 503)

        json_response = response.json()

        if response.status_code in self.__ERROR_CODES:
            error_message = (
                f"Response error from Shopify. Status code {response.status_code}. "
                f"Error message: {json_response['errors']}"
            )

            raise ShopifyManagerException(error_message, response.status_code)

        LOGGER.info(f"Shopify response: Status code {response.status_code}. Message: {response.text}")

        return json_response

    def do_get(self, endpoint, payload=None):

        response = self.make_request(method=self._GET, endpoint=endpoint, payload=payload)

        return self.handle_response(response)
