from .shopify_manager import ShopifyManager


class Customers(ShopifyManager):
    def get_count(self):
        endpoint = "customers/count.json"

        return self.do_get(endpoint)
