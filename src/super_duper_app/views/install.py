from django import views, http
from django.conf import settings

import utils

API_KEY = settings.SHOPIFY_API_KEY


class Install(views.View):
    redirect_uri = f"{settings.BASE_URL}/api/callback"

    def get(self, *args, **kwargs):
        store_domain = kwargs.get("store_domain")
        state = utils.get_nonce()
        install_url = self.get_install_url_for_store(store_domain, state)

        response = http.HttpResponseRedirect(redirect_to=install_url)
        response.set_signed_cookie("state", state)

        return response

    def get_install_url_for_store(self, store_domain, state):
        scope = ", ".join(settings.APPLICATION_REQUIRED_SCOPES)
        return (
            f"https://{store_domain}/admin/oauth/authorize?"
            f"client_id={API_KEY}&scope={scope}&state={state}"
            f"&redirect_uri={self.redirect_uri}"
        )
