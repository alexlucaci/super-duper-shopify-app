import hmac
import hashlib
import requests

from django import views, http
from django.conf import settings
from rest_framework import exceptions

import shopify_manager
import twilio_manager

API_KEY = settings.SHOPIFY_API_KEY
API_SECRET_KEY = settings.SHOPIFY_API_SECRET_KEY


class Callback(views.View):
    def get(self, *args, **kwargs):
        query_params = self.request.GET.dict()

        self.__validate_shopify_request(query_params)

        shop = query_params.get("shop")
        code = query_params.get("code")

        access_token = self.get_access_token_for_store(shop, code)

        if access_token is None:
            raise exceptions.APIException("Something went wrong. Access token was not able to retrieve")

        shop_info = self.get_shop_info(shop, access_token)

        greeting_message = (
            f'Hi {shop_info["first_name"]}, it\'s great to see {shop_info["store_name"]} '
            f'installed with our app. {shop_info["customer_count"]} customers will love it.'
        )

        twilio = twilio_manager.TwilioManager()
        message_status = twilio.send_message(body=greeting_message, to_number=shop_info["phone"]).status

        # Commented out the code when logic is implemented with celery taska after 5 mins (60 seconds * 5)

        # message_status = "queued"
        # twilio_manager.send_message.apply_async((greeting_message, shop_info["phone"]), countdown=60*5)

        return http.JsonResponse(
            {"greeting_message": greeting_message, "phone_number": shop_info["phone"], "message_status": message_status}
        )

    def __validate_shopify_request(self, query_params):
        state_from_cookie = self.request.get_signed_cookie("state")

        shop = query_params.get("shop")
        original_hmac = query_params.get("hmac")
        code = query_params.get("code")
        state = query_params.get("state")

        if state != state_from_cookie:
            raise exceptions.APIException("Invalid request. Cookies were tampered!")

        if not (shop and original_hmac and code):
            raise exceptions.APIException(
                "Invalid request. One or more query params (shop, hmac, code) were not provided!"
            )

        # Preparing message from query params to digitally sign in order to verify it
        # So we need to delete the already existing hmac key from it in order to sign the rest
        del query_params["hmac"]

        message = self.compute_message_from_params(query_params)

        if not self.is_hmac_valid(original_hmac, message):
            raise exceptions.APIException("Invalid request. Hmac is invalid!")

    @staticmethod
    def get_shop_info(store, access_token):
        store_properties = shopify_manager.StoreProperties(store=store, access_token=access_token).get_properties()

        customer_count = shopify_manager.Customers(store=store, access_token=access_token).get_count()

        # shop owner named is stored like a full name: "Alex Lucaci" so split by " " is required
        first_name = store_properties.get("shop_owner").split(" ")[0]

        # Set a random fallback number to send sms if the store is not providing any number
        fallback_number = "+40799999999"

        phone = store_properties.get("phone") or fallback_number

        return {
            "first_name": first_name,
            "store_name": store_properties.get("name"),
            "phone": phone,
            "customer_count": customer_count.get("count"),
        }

    @staticmethod
    def get_access_token_for_store(shop, code):
        access_token_request_url = f"https://{shop}/admin/oauth/access_token"
        payload = {"client_id": API_KEY, "client_secret": API_SECRET_KEY, "code": code}
        response = requests.post(access_token_request_url, json=payload).json()
        access_token = response.get("access_token")

        return access_token

    @staticmethod
    def compute_message_from_params(query_params):
        """ Create a query params message like with sorted keys from a given dict

        Ex: from: {"state": 0493234234, "code": 123456} -> "code=123456&state=0493234234"
        :param query_params: dict
        :return: str
        """
        return "&".join([f"{key}={value}" for key, value in sorted(query_params.items())])

    @staticmethod
    def is_hmac_valid(original_hmac, message):
        computed_hmac = hmac.digest(
            key=API_SECRET_KEY.encode("utf-8"), msg=message.encode("utf-8"), digest=hashlib.sha256
        ).hex()
        return hmac.compare_digest(computed_hmac, original_hmac)
