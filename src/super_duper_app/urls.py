from django.conf.urls import url
from super_duper_app import views

urlpatterns = [
    url(r"^login/$", views.Login.as_view(), name="login"),
    url(r"^install/(?P<store_domain>[\w\-_.]+)/$", views.Install.as_view(), name="install-store"),
    url(r"^callback", views.Callback.as_view(), name="callback"),
]
