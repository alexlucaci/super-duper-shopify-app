from django.conf import settings
from twilio import rest
from rest_framework import exceptions
from celery import shared_task

TWILIO_TEST_SID = settings.TWILIO_TEST_SID
TWILIO_TEST_AUTHTOKEN = settings.TWILIO_TEST_AUTHTOKEN


@shared_task()
def send_message(body, to_number, from_number="+15005550006"):
    twilio = TwilioManager()
    twilio.send_message(body, to_number, from_number)


class TwilioManager:
    """
    Class that implements twilio actions.
    Methods can also be implemented as celery tasks
    """

    def __init__(self):
        self.client = rest.Client(TWILIO_TEST_SID, TWILIO_TEST_AUTHTOKEN)

    def send_message(self, body, to_number, from_number="+15005550006"):
        """
        :param to_number: number to sent the sms
        :param from_number: number from where the sms is sent - defaults to +15005550006 which
        is valid number for test credentials
        :return: message.sid dict - info about the sms sent
        """
        message = self.client.messages.create(body=body, from_=from_number, to=to_number)

        if message.error_code is not None:
            raise exceptions.APIException(detail=message.error_message, code=message.error_code)

        return message
